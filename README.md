# isa-k8s

## Co trzeba przygotować przed
- konto w hub.docker.com 
- konto gitlab.com 

Do pracy z klastrem:
- AWS-CLI: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
- Kubectl: https://kubernetes.io/docs/tasks/tools/#kubectl
- Opcjonalnie: kubens i kubectx: https://github.com/ahmetb/kubectx

AWS-CLI powinniście mieć już skonfigurowane. 

## Konfiguracja kubectl:
W konsoli wydać polecenie:
`aws eks update-kubeconfig --region eu-west-1 --name managed_node_groups-a8SIM29L`
Dzięki temu kubectl zostanie skonfigurowany w oparciu o wasze dostępy AWS'owe

Przetestować działanie kubectl można wydająć polcenie:
`kubectl get svc`
Wynik takiego polecenie powinien wyglądać mniej więcej tak:

```
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   172.20.0.1   <none>        443/TCP   16h
```

Kubectx przydaje się, jeśli już korzystacie z innych klastrów kubernetes. 
W takim wypadku to polecenie pomoże wam przełączać się między kontekstami
Polecenie: `kubectl config get-contexts` listuje wszystkie skonfigurowane konteksty
Polecenie: `kubectx _nazwa_kontekstu_` przełącza na wybrany kontekst

## Przygotowanie obrazu
1. Wykonać fork repozytorium do swojego konta (przycisk "fork" po prewej stronie na górze strony)
2. Sklonować repozytorium lokalnie 
3. Zalogować się do swojego konta na hub.docker.com wydając w konsoli polecenie `docker login`. 
4. Po poprawnej autoryzacji zbudować i wypushować obraz (polcenie wydać z poziomu głównego katalogu aplikacji)

```shell
docker build . -t __wasze_konto_w_docker__/__nazwa_aplikacji__:latest
docker image push __wasze_konto_w_docker__/__nazwa_aplikacji__:latest
```

Na końcu warto zweryfikować czy obraz faktycznie jest widoczny w docker 
(przykład mojego obrazu: https://hub.docker.com/r/mgiergielewicz/hello-world/tags)
Wasz obraz otagowany jako latest posłuży do początkowego uruchomienia aplikacji

## Zmiany w kodzie aplikacji

1. Zmodyfikować .gitlab-ci.yml
   1. W jobie budowania obrazu zmienić nazwę obrazu tak, aby wskazywała na ten który zbudowaliście
      1. Ważne, aby zachować tagowanie przez $CI_COMMIT_SHORT_SHA
   2. W jobie deplyowania aplikacji dostosować polecenie tak, aby:
      1. parametr -n (namespace) wskazywał wasz namespace, który ustawiliście zgodnie z opisem w następnym punkcie (pkt2.1)
      2. obraz wstawiany w deployment był waszym obrazem otagowanym tagiem na podstawie $CI_COMMIT_SHORT_SHA
      3. Jeżeli zmienialiście nazwę deploymentu, to też trzeba w tym miejscu tą zmianę uwzględnić
2. Zmodyfikować pliki w katalogu kubernetes: 
      1. namespace.yaml - ustawić unikalny namespace dla swojej aplikacji (np. niech zawiera wasze imie i nazwisko)
      2. deployment.yaml - zmienić nazwę obrazu zgodnie z tym który wybraliście w jobie budowania


## Ręczny początkowy deploy
Na początek aplikacja zostanie postawiona ręcznie (job w pipeline tylko aktualizuje istniejącą aplikację)
```shell
kubectl apply -f kubernetes/namespace.yaml
kubectl -n _namespace_ apply -f kubernetes/service.yaml
kubectl -n _namespace_ apply -f kubernetes/deployment.yaml
```
Ważne, aby pamiętać o parametrze -n (namespace) i ustawić tam ten namespace, który ustawiliście w pliku namespace.yaml

Aplikacja powinna wstać w kilkanaście sekund. Kilka poręcznych poleceń do sprawdzania stanu:

`kubectl -n _namespace_ get pod` - listujemy pody w namespace

`kubectl -n _namespace_ describe pod _nazwa_poda_` pokazujemy detale poda - tutaj warto zerknąć na sekcję 'events' i zobaczyć czy nie ma tam jakichś komunikatów o błędach

Jeśli aplikacja została poprawnie uruchomiona, trzeba wykonać polecenie `kubectl -n _namepsace_ get svc` i z kolumny EXTERNAL-IP skopiować adres i wkleić do przeglądarki www. 
Kilka minut po uruchomieniu aplikacji ten adres zacznie działać (propagacja dns) i powinniście zobaczyć w przeglądarce zawartość pliku public/index.html

Następnie można przejść do testu deploymentu:

## Konfiguracja deploymentu

1. Dodanie zmiennych środowiskowych z credential'ami do dockera i AWS
   1. Zmienne dodaje się w gitlabie sekcji Settings -> CI / CD w obszarze Variables
   2. Zmienne, które trzeba ustawić:
      1. AWS_ACCESS_KEY_ID
      3. AWS_SECRET_ACCESS_KEY
      2. AWS_DEFAULT_REGION (tutaj zawsze ustawiamy: eu-west-1)
      5. DOCKER_USER (user do logowania do hub.docker.com)
      4. DOCKER_PASSWORD (hasło do hub.docker.com)

2. Deploy kodu
   1. Możemy przystąpić do deployou. Wykonujemy zmianę w pliku public/index.html, commitujemy zawartość repozytorium i wykonujemy push'a
   2. Na gitlab.com w sekcji CI/CD -> Pipelines możemy obserwować proces wykonywania jobów (budowanie obrazu i deploy)
   3. Po zakończeniu deployou możemy wejść na wcześniej skopiowny url i potwierdzić czy zmiana w index.html została wrzucona na środowisko


## Przykłady i materiały dla Dockerfile ##
 * https://docs.docker.com/language/
 * https://www.docker.com/blog/containerized-python-development-part-1/
 * https://www.digitalocean.com/community/tutorials/how-to-build-a-node-js-application-with-docker

